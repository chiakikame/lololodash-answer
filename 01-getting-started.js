const _ = require("lodash");

function worker(users) {
  return _.filter(users, {active: true});
}

module.exports = worker;
