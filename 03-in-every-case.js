const _ = require('lodash');

function worker(items) {
  return _.forEach(items, (item) => {
    const pop = item.population;
    
    let size = '';
    if (pop >= 1.0) {
      size = 'big';
    } else if (pop >= 0.5) {
      size = 'med';
    } else {
      size = 'small';
    }
    
    item.size = size;
  });
}

module.exports = worker;
