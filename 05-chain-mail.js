const _ = require('lodash');

function worker(data) {
  return _.chain(data)
    .map((str) => str.toUpperCase() + "CHAINED")
    .sort();
}

module.exports = worker;
