const _ = require('lodash');

function worker(data) {
  const townClassify = {
    hot: [],
    warm: []
  };
  
  const condition = elem => elem > 19;
  
  _.forEach(data, (townTemperatureData, townName) => {
    if (_.every(townTemperatureData, condition)) {
      townClassify.hot.push(townName);
    } else if (_.some(townTemperatureData, condition)) {
      townClassify.warm.push(townName);
    }
  });
  
  return townClassify;
}

module.exports = worker;
