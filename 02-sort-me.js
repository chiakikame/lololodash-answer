const _ = require('lodash');

function worker(items) {
  return _.sortBy(items, (elem) => -elem.quantity);
}

module.exports = worker;
