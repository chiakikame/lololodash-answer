const _ = require('lodash');

function worker(data) {
  const totalIncome = _.reduce(data, (acc, elem) => acc + elem.income, 0);
  const size = _.size(data);
  const averageIncome = totalIncome / size;
  
  const underPerformers =
    performerPipe(data, elem => elem.income <= averageIncome);
    
  const overPerformers =
    performerPipe(data, elem => elem.income > averageIncome);
  
  return {
    average: averageIncome,
    underperform: underPerformers,
    overperform: overPerformers
  };
}

function performerPipe(data, filterCondition) {
  return _.chain(data)
    .filter(filterCondition)
    .sortBy('income');
}

module.exports = worker;
