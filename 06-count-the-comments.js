const _ = require('lodash');

function worker(data) {
  return _.chain(data)
    .groupBy('username')
    .map((e, k) => {
      return { username: k, comment_count: _.size(e) };
    })
    .sortBy(e => -e.comment_count);
}

module.exports = worker;
