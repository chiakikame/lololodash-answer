const _ = require('lodash');

function worker(data) {
  return _.chain(data)
    .groupBy('article')
    .map((elem, key) => {
      return {
        // I don't get it: how come the key becomes a string
        article: parseInt(key),
        total_orders: _.reduce(elem, (acc, val) => acc + val.quantity, 0)
      };
    })
    .sortBy(elem => -elem.total_orders);
}

module.exports = worker;
