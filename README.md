This is a personal answer repo for `lololodash` workshop.

## Important Aspects

* `filter`
* `sortBy` with field key or callback
* `forEach`, which allows modification of iterated elements
* `every` and `some`
* Chain actions with help of `chain` function
* `groupBy` and `size`
* `reduce`
* `template`

## Executing problem

If there's syntax error in submitted code, it would report:

```
✗ Could not find your file. Make sure the path is correct.
```

which I humbly consider a bug.
