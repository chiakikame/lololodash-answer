const _ = require('lodash');

function worker(data) {
  const loginCount = _.size(data.login);
  const name = data.name;
  const templateString = 'Hello <%= name %> (logins: <%= loginCount %>)';
  const calculatedData = {
    name: name,
    loginCount: loginCount
  };
  
  return _.template(templateString)(calculatedData);
}

module.exports = worker;
